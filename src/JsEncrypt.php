<?php
namespace Leroi\JsEncrypt;

use Leroi\StaticInstance\StaticInstance;

class JsEncrypt
{

    use StaticInstance;

    public function encode($input=""){
        if (!is_string($input)){
            return false;
        }
        $staticchars = "PXhw7UT1B0a9kQDKZsjIASmOezxYG4CHo5Jyfg2b8FLpEvRr3WtVnlqMidu6cN";
        $encodechars = "";
        for ($i=0;$i<strlen($input);$i++){
            $num0 = strpos($staticchars,$input[$i]);
            if($num0 == -1){
                $code = $input[$i];
            }else{
                $code = $staticchars[($num0+3)%62];
            }
            $num1 = mt_rand(0,61);
            $num2 = mt_rand(0,61);
            $encodechars .= $staticchars[$num1].$code.$staticchars[$num2];
        }
        return $encodechars;
    }


    public function decode($input=""){
        if (!is_string($input)){
            return false;
        }
        $staticchars = "PXhw7UT1B0a9kQDKZsjIASmOezxYG4CHo5Jyfg2b8FLpEvRr3WtVnlqMidu6cN";
        $decodechars = "";
        for($i=1;$i<strlen($input);){
            $num0 = strpos($staticchars, $input[$i]);
            if($num0 !== false){
                $num1 = ($num0+59)%62;
                $code = $staticchars[$num1];
            }else{
                $code = $input[$i];
            }
            $decodechars .= $code;
            $i+=3;
        }
        return $decodechars;
    }

}
