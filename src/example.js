function encode(_str){
    var staticchars = "PXhw7UT1B0a9kQDKZsjIASmOezxYG4CHo5Jyfg2b8FLpEvRr3WtVnlqMidu6cN";
    var encodechars = "";
    for(var i=0;i<_str.length;i++){
        var num0 = staticchars.indexOf(_str[i]);
        if(num0 == -1){
            var code = _str[i];
        }else{
            var code = staticchars[(num0+3)%62];
        }
        var num1 = parseInt(Math.random()*62,10);
        var num2 = parseInt(Math.random()*62,10);
        encodechars += staticchars[num1]+code+staticchars[num2];
    }
    return encodechars;
}

function decode(_str){
    var staticchars = "PXhw7UT1B0a9kQDKZsjIASmOezxYG4CHo5Jyfg2b8FLpEvRr3WtVnlqMidu6cN";
    var decodechars = "";
    for(var i=1;i<_str.length;){
        var num0 = staticchars.indexOf(_str[i]);
        if(num0 == -1){
            var code = _str[i];
        }else{
            var code = staticchars[(num0+59)%62];
        }
        decodechars += code;
        i = i+3;
    }
    return decodechars;
}